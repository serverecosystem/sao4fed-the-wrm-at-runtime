# Server Add-Ons for Front-End Developers Tutorial
## Leaning on the Web Resource Manager (WRM) at runtime

This tutorial aims to give you an understanding of the front-end capabilities of an Atlassian Server add-on, as provided by [Atlassian's Web Resource Manager (aka: the WRM)][10].

## Tutorial

By completing this tutorial, you will be able to:

* Push assets to the front-end of an Atlassian Server web product through your add-on.
* Lazily load your application code.
* Translate your application in to multiple languages.
* Conditionally output resources to the browser.
* Take advantage of the WRM's runtime support of the [LESS css preprocessor][201].
* Use the WRM's runtime support of [Google Closure Templates][202], also known as Soy templates.

### The purpose of the WRM

Any website or web application works by serving pages or assets from a URL.
The web server needs to know how to route those URLs to the appropriate content to respond with.

On a basic website, this would typically involve serving a static HTML file,
which would include hard-coded URLs to your CSS and JavaScript files, which were also static.
More advanced websites might use a pre-processor to compile those HTML, CSS and JavaScript files.
In either case, the end result is the same: the route from URL to content is just a 1-to-1
mapping to the web server's file system.

In web applications with server-side logic, the routing from URL to content can be more
complex. The server application's container will intercept requests to the website, pass them to the
server application, which can decide what to serve in response. The file system may not be involved,
or may not even be accessible to the server application.

In Atlassian products, the filesystem is effectively inaccessible. What's more, when you build an
Atlassian add-on, you are providing a single file -- typically a plugin JAR -- that contains your
front-end code (amongst other things). So, an Atlassian product needs a way of converting a request
for a URL in to finding your application's code and serving it in response.

This is the job the Web Resource Manager performs.

### Utilising the WRM's feature set

We've broken down this repository in to a number of demonstrations of each different capability of the WRM,
based on the goals you want to achieve.

* [I want my application code to load after the initial page does](examples/lazy-load-resources).
* [I want to translate the UI rendered by our front-end application](examples/translate-resources).
* [I want some data to be available immediately without a round-trip to the server](examples/seed-data-injection).
* [I want some of my code to only appear in the application if a condition is met](examples/conditionally-include-resources).
* [I want to debug my production code in the browser using source maps](examples/serve-sourcemaps).

## Next steps

Want to know more about the other pieces of the Atlassian P2 plugin system? [The front-end pieces of an add-on][301] tutorial covers this.

Want to learn how to leverage newer versions of JavaScript and modern front-end frameworks when building your add-on? The [compiling the UI tutorial][302] will help you leverage those technologies while building your Server add-on.

Want to build an optimal and performant front-end by taking advantage of a module system for front-end code (for example: ES6 Modules; AMD; UJS; etc.)? Consult the [Bundling the UI tutorial][303].


[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
[10]: https://bitbucket.org/atlassian/atlassian-plugins-webresource
[201]: http://lesscss.org/
[202]: https://developers.google.com/closure/templates/
[301]: https://bitbucket.org/serverecosystem/sao4fed-the-frontend-pieces-of-an-addon
[302]: https://bitbucket.org/serverecosystem/sao4fed-compile-the-ui
[303]: https://bitbucket.org/serverecosystem/sao4fed-bundle-the-ui