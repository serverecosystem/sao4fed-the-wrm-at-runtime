# Leaning on the WRM at runtime
## Translating your add-on

The Web Resource Manager provides facilities for translating your add-on in to your user's language.
These facilities are invoked at an Atlassian web product's runtime, which means you don't need to
pre-compile your application in to all the potential languages your end-users have, and your
translations can change and evolve over time without needing to rebuild your add-on.

There is an excellent step-by-step guide on [developer.atlassian.com][1] for
[internationalising your add-on][301], which covers the fundamentals
of the translation facilities provided by the WRM.


[1]: https://developer.atlassian.com
[301]: https://developer.atlassian.com/docs/common-coding-tasks/internationalising-your-plugin