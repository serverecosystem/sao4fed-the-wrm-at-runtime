# Leaning on the WRM at runtime
## Making data available to a page immediately

Sometimes, you have data you want to provide to your UI code immediately, without
having to ask the server for it and suffer the AJAX round-trip to retrieve it. A common
example scenario for wanting this behaviour is seeding the initial values of a typeahead
UI control; you want to ensure that your end-user has something to interact with
regardless of how fast or slow the server responds to them, so you seed some of the
data up-front.

The Web Resource Manager allows you to seed data through a concept called a "data provider".

There is a tutorial on [developer.atlassian.com][1] on
[adding data providers to your plugin][2]. Here, we'll go through the process too.

## How to do it

Data providers can be implemented in just a few short steps.

### Step 1: create a `WebResourceDataProvider`

Data providers are implemented by extending the `WebResourceDataProvider` Java class.
In your add-on codebase, you can add a class like the following:

```java
package my.plugin.package;

public class MyDataProvider extends WebResourceDataProvider {
    @Override
    public Jsonable get() {
        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                Gson gson = new Gson();
                gson.toJson(sampleData(), Map.class, new PrintWriter(writer));
            }
        };
    }

    public Map<String, Object> sampleData() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "Fido");
        map.put("species", "Canine");
        map.put("breed", "Shiba Inu");
        return map;
    }
}
```

### Step 2: expose the data in the UI

Next, you edit your `atlassian-plugin.xml` file and reference the data provider class from a `<web-resource>` block:

```xml
<atlassian-plugin key="my.plugin.key" name="${project.name}" plugins-version="2">
  <plugin-info>...</plugin-info>

  <web-resource key="my-ui-thing">
    <resource type="download" name="our-javascript.js" location="our-module.js"/>

    <!-- Allow scripts within this web-resource to claim the data we provide through it -->
    <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:data</dependency>
    <!-- More <resource>s, <dependency>s, <context>s, etc. etc. -->

    <data key="whatever-my-data-is-called" class="my.plugin.package.MyDataProvider" />
  </web-resource>

  <!-- and so on... -->
</atlassian-plugin>
```

### Step 3: claim the data

Now that the Atlassian Server product knows how to serve your data, it's time for you to
"claim" the data on the client-side.

```js
define('myplugin/our-module', ['wrm/data'], function(wrmData) {
    // You retrieve your data via the "claim" function of the wrm/data module.
    //
    // The data is referenced by string. The format of the string is
    // '<plugin-key>:<web-resource-key>.<data-provider-key>'
    var ourData = wrmData.claim('my.plugin.key:my-ui-thing.whatever-my-data-is-called');

    console.log(ourdata instanceof Object); // prints true
    console.log(ourData.hasOwnProperty('name')); // prints true
    console.log(ourData.name); // prints 'Fido'

    // Yay, you has data!
});
```

And just like that, you has data.


## Alternative approaches and trade-offs

There are other common patterns for serving data that should be refactored to use Data providers.

### Adding data in to `data-` attributes on arbitrary HTML elements

When rendering your templates, you could choose to push data in to an HTML attribute on
one of the elements your template renders. That way, the data is available in your DOM where
your client-side logic can pick it up through some DOM queries.

Sometimes this approach the right thing to do. For instance, you may be building or
using web components, and by pushing the data in to the web components' declarative HTML attribute API,
you avoid having to do so at runtime, so the element starts working sooner. Great!

In other cases, using a Data provider is superior because:

1. You don't need to worry about HTML-escaping your data or preventing XSS attacks through your templates.
   When you use a data provider, the data is output in a JSON format and claimed by a JavaScript API,
   so the DOM never gets involved.

2. They can't slow down page rendering.
   When you serve data in HTML attributes, they become part of the DOM tree. When a browser renders
   an HTML page, the DOM tree is combined with the Style tree (as derived from your CSS) to produce
   the Render tree (what the user ultimately sees).
   It follows that your HTML attributes could be hooked by your CSS styles. This is why whenever the
   attribute value changes, that element and all its children need to be re-rendered in the browser.
   By using a data provider, your data doesn't form part of the DOM tree, so cannot affect render times.

### Requesting data via a REST endpoint

There's certainly nothing wrong with retrieving data via a REST endpoint. In a majority of cases,
serving data via a REST endpoint is a superior solution to use of a data provider.

A data provider is better than a REST endpoint **only if** you need the data available in the UI
as soon as possible, and waiting for a round-trip to the server for a REST request would be
too slow for that purpose.

A data provider is *worse* than a REST endpoint if:

* You need to make multiple requests for data.
  Because data providers include their data in the `<head>` of the HTML response to the end-user,
  by their nature, they can only provide a static response. If the state of your application
  changes, and this would invalidate or significantly change the data your UI requires,
  you will still need to request that data through an asynchronous REST endpoint.

* You have a *whole lot* of data to transfer between the server and client.
  In such cases, it's better to build your UI to work lazily and asynchronously, such that
  your time to first paint is lower, and you can progressively fetch and render data from
  the server as you need to.


[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin
