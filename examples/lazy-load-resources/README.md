# Leaning on the WRM at runtime

## Lazy loading resources

Part of the challenge of developing for the web is ensuring the initial page load remains
small and the UI is rendered as soon as possible, so the end-user can start getting
value out of the page content as early as possible.

Often, feature code isn't necessary immediately upon a page loading.
Rather, there's a tiny set of code that you need to load up-front, but the rest can come in at a later
point in time, closer to when the end-user actually needs the behaviour.

To help you keep initial page size small and unblock initial page rendering,
the WRM provides an ability to lazily load resources via JavaScript, and react to their loading.

## How to do it

### Step 1: gathering your lazy assets

To start with, you'll need to put the resources you want lazy-loaded in a `web-resource`.

```xml
<atlassian-plugin key="my.plugin.key" name="${project.name}" plugins-version="2">

  <web-resource key="my-lazy-feature">
    <resource type="download" name="my-lazy-feature.js" location="feature-code.js"/>
  </web-resource>
</atlassian-plugin>
```

In this "my-lazy-feature" `web-resource`, you can add all JavaScript, CSS, images, and other resources
you need loaded in order to support and run the feature, as well as any `<dependency>` declarations
on other `web-resource`s that should be loaded too.

### Step 2: create an initialiser `web-resource`

Next, create a new web-resource with a single javascript file and no `<dependency>` declarations.

```xml
<atlassian-plugin key="my.plugin.key" name="${project.name}" plugins-version="2">

  <web-resource key="my-lazy-feature">...</web-resource>
  
  <web-resource key="init-my-app">
    <!-- I have no runtime deps** -->
    <resource type="download" name="init-my-app.js" location="init.js"/>
  </web-resource>
</atlassian-plugin>
```

This "init-my-app" `web-resource` will be responsible for starting up our application's features,
which will involve invoking the command that loads the "my-lazy-feature" code.

### Step 3: add a `<dependency>` on the WRM require module

To your "init-my-app" `web-resource`, add a dependency on the
`com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager`:

```xml
<atlassian-plugin key="my.plugin.key" name="${project.name}" plugins-version="2">

  <web-resource key="my-lazy-feature">...</web-resource>
  
  <web-resource key="init-my-app">
    <dependency>com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager</dependency>
    <resource type="download" name="init-my-app.js" location="init.js"/>
  </web-resource>
</atlassian-plugin>
```

This will allow the initialisation code to make use of the JavaScript API for lazy-loading
`web-resource` definitions.

### Step 4: Declare when to lazily load your code

In the `init.js` file, you can make use of the `wrm/require` AMD module or `WRM.require` global
to declare when to load your feature.

```js
// init.js
require(['wrm/require', 'jquery'], function(WrmRequire, $) {
    // First, we'll define a function to wrap the loading of our feature code.
	function lazyLoadTheFeature() {
	    WrmRequire(['wr!my.plugin.key:my-lazy-feature'], function loadingDone() {
            // Our feature's assets are now loaded on to the page.
            // Thanks, WRM!
        });
    }

    // This function can then be invoked at any point in time. For example,
	// When a particular button is clicked or focussed.
	$(document).on('focus hover', '#my-feature-button', lazyLoadTheFeature);
});
```

Now, whenever a button with the `my-feature-button` ID is focussed or hovered on the page,
the feature code will lazily load.

Once the feature code is present on the page, the `loadingDone` function will be called.
If the feature code was already present on to the page, this is called on the next tick
in JavaScript -- in other words, it is not a synchronous callback.

### Step 5: Add the "init-my-app" `web-resource` to a product page

The app initialisation code will only run if you include it on a page. You may
choose how to achieve this -- whether by adding the init code to a `context`,
declaring a `<dependency>` on it from another `web-resource` that's already loaded on a page,
or making an explicit `requireResource` call on the server-side for it.

## Alternative approaches and trade-offs

There are two important considerations you must make:

1. Choosing what should load lazily, and
2. How early or late should you load it.

Loading feature code immediately and synchronously with the page is straightforward,
but means all the cost of downloading, parsing an executing your feature code is incurred
in the initial page load. The feature code may not be used or needed by the end-user until
much later, or even at all, but they will still pay the cost and have to wait for the
browser to run your code. Making the end-user wait is bad, both from the point of respecting
their time, and the negative impression of application slowness it leaves on them.

If you only begin the task of loading your feature at the point the end-user expresses
their intent of using your feature, the end-user will have to wait for the feature code to
download, parse, and execute. This means the responsiveness of the feature would
be based on how fast the end-user's network connection was, as well as how quickly their
browser can download, parse, and execute the feature code. In such a case, it would be up to you to
provide an appropriate intermediary loading state to demonstrate that the user's action
was noticed and the system is doing something.

The ideal middle-ground between these two extremes is to load your code in the downtime between the initial
page load and the end-user wanting to use your feature. At the time of writing, there are no
API calls provided by Atlassian to ease the automatically determine and load things in this downtime.
