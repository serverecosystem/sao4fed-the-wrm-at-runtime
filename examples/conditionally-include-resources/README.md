# Leaning on the WRM at runtime
## Conditionally including resources

The Web Resource Manager makes it possible for you to include your resources in the page
when specific conditions are met -- for example, if your add-on is licensed, or if the
logged-in user is an administrator, or ... just about anything you can determine at runtime
on the server at the time of a URL being requested.

There is an excellent step-by-step guide on [developer.atlassian.com][1] for
[omitting web-resources when your add-on is unlicensed][301], which covers the fundamentals
of conditionally including or excluding `<web-resource>`s from pages.

[1]: https://developer.atlassian.com
[301]: https://developer.atlassian.com/market/add-on-licensing-for-developers/tutorial-adding-licensing-support-to-your-add-on/tutorial-omit-web-resources-when-your-plugin-is-unlicensed
